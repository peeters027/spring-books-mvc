package pl.spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.spring.demo.constants.ModelConstants;
import pl.spring.demo.constants.ViewNames;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;

/**
 * Book controller
 * 
 * @author mmotowid
 *
 */
@Controller
@RequestMapping("/books")
public class BookController {

	@Autowired
	private BookService bookService;

	
	@RequestMapping
	public String list(Model model) {
		return "redirect: /webstore/books/all";
	}

	/**
	 * Method collects info about all books
	 * 
	 * @return model and view of the page
	 */
	@RequestMapping("/all")
	public ModelAndView allBooks() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ViewNames.BOOKS);
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookService.findAllBooks());
		return modelAndView;
	}

	/**
	 * Method collects details about specified book
	 * 
	 * @param id
	 *            the id of the book
	 * @return model and view of the page
	 */
	@RequestMapping("/book")
	public ModelAndView showDetails(Long id) {
		ModelAndView modelAndView = new ModelAndView();

		BookTo book = bookService.findBookById(id);
		if (book != null) {
			modelAndView.setViewName(ViewNames.BOOK);
			modelAndView.addObject(ModelConstants.BOOK, book);
		} else {
			modelAndView.setViewName(ViewNames._403);
			modelAndView.addObject(ModelConstants.ERROR_MESSAGE, "This book doesn't exist.");
		}
		return modelAndView;
	}

	/**
	 * Method collects info about all books found by title or author
	 * 
	 * @param title
	 *            the title of the book
	 * @param author
	 *            the author of the book
	 * @return model and view of the page
	 */
	@RequestMapping("/found")
	public ModelAndView searchBooksByTitleOrAuthor(@RequestParam("titlePrefix") String title,
			@RequestParam("authorPrefix") String author) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ViewNames.BOOKS);
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookService.findBooksByAuthorOrTitle(author, title));
		return modelAndView;
	}

	/**
	 * Method removing book from database and showing info about removed book
	 * 
	 * @param id
	 *            the id of the removing book
	 * @return model and view of the page
	 */

	@RequestMapping("/remove")
	public ModelAndView removeBook(Long id) {
		ModelAndView modelAndView = new ModelAndView();
		BookTo book = bookService.findBookById(id);
		if (book != null) {
			bookService.deleteBook(id);
			modelAndView.setViewName(ViewNames.REMOVED_BOOK);
			modelAndView.addObject(ModelConstants.BOOK, book);
		} else {
			modelAndView.setViewName(ViewNames._403);
			modelAndView.addObject(ModelConstants.ERROR_MESSAGE, "This book doesn't exist.");
		}
		return modelAndView;
	}

	/**
	 * Get method for adding book to the database
	 * 
	 * @param model
	 *            the model of the attribute
	 * @return view for adding book
	 */

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addBook(Model model) {
		model.addAttribute("newBook", new BookTo());
		return ViewNames.ADD_BOOK;
	}

	/**
	 * Post method for adding book to the database
	 * 
	 * @param newBook
	 *            the book that is going to be added
	 * @return model and view of the page
	 */

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addBook(@ModelAttribute("newBook") BookTo newBook) {
		if (!(newBook.getAuthors() == "" || newBook.getTitle() == "")) {
			BookTo book = bookService.addNewBook(newBook);
			bookService.saveBook(book);
		}
		bookService.findAllBooks();
		ModelAndView modelAndView = new ModelAndView(ViewNames.BOOKS);
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookService.findAllBooks());
		return modelAndView;
	}

	/**
	 * Binder initialization
	 */
	@InitBinder
	public void initialiseBinder(WebDataBinder binder) {
		binder.setAllowedFields("id", "title", "authors", "status");
	}

}
