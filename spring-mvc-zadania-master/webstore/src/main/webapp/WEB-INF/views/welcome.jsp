<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Hello</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>${greeting}</h1>
				<p>${info}</p>
			</div>
		</div>
	</section>
	<section>
	<div class="row">
	<div class="col-sm-2 col-sm-offset-8" style="padding-bottom: 30px">
			<div class="thumbnail">
				<div class="caption">
					<sec:authorize access="!isAuthenticated()">
					<p>
							<a href="/webstore/login" class="btn btn-default"> <span
								class="glyphicon glyphicon-user" /></span> LOGIN
							</a>
						</p>
					</sec:authorize>
					<sec:authorize access="isAuthenticated()">
						<p>
							<a href="/webstore/logout" class="btn btn-default"> <span
								class="glyphicon glyphicon-user" /></span> LOGOUT
							</a>
						</p>
					</sec:authorize>
				</div>
			</div>
			</div>
		</div>
	</section>
	<section>
		<div class="col-sm-2 col-sm-offset-1" style="padding-bottom: 15px">
			<div class="thumbnail">
				<div class="caption">
					<h3>Books</h3>
					<p>Display all books</p>
					<p>
						<a href="/webstore/books/all" class="btn btn-default"> <span
							class="glyphicon-info-sign glyphicon" /></span> Show all books
						</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-2 col-sm-offset-1" style="padding-bottom: 15px">
			<div class="thumbnail">
				<div class="caption">
					<h3>Add book</h3>
					<p>Create new book</p>
					<p>
						<a href="/webstore/books/add" class="btn btn-default"> <span
							class="glyphicon-info-sign glyphicon" /></span> Add book
						</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 col-sm-offset-2" style="padding-bottom: 15px">
			<div class="thumbnail">
				<div class="caption">
					<form action="/webstore/books/found">
						Title:<br> <input type="text" name="titlePrefix" value="">
						<br> Written by:<br> <input type="text"
							name="authorPrefix" value=""> <br> <br>
						<button type="submit">Search books</button>
					</form>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
