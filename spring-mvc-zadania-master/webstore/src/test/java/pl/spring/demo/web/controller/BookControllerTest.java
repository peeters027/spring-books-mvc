package pl.spring.demo.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pl.spring.demo.constants.ModelConstants;
import pl.spring.demo.controller.BookController;
import pl.spring.demo.enumerations.BookStatus;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(MockitoJUnitRunner.class)
public class BookControllerTest {

	private MockMvc mockMvc;

	@Mock
	private BookService bookService;

	@InjectMocks
	private BookController bookController;

	@Before
	public void setup() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix("jsp");

		this.mockMvc = MockMvcBuilders.standaloneSetup(bookController).setViewResolvers(viewResolver).build();
	}

	@Test
	public void shouldTestGetForShowAllBooks() throws Exception {

		// given
		List<BookTo> listOfBooks = new ArrayList<BookTo>();
		Mockito.when(bookService.findAllBooks()).thenReturn(listOfBooks);

		// when
		ResultActions resultActions = mockMvc.perform(get("/books/all"));

		// then
		resultActions.andExpect(view().name("books"))
				.andExpect(model().attribute(ModelConstants.BOOK_LIST, listOfBooks));
	}

	@Test
	public void shouldTestGetForBookDetails() throws Exception {

		// given
		BookTo bookInBase = new BookTo();
		bookInBase.setTitle("title");
		Mockito.when(bookService.findBookById(1L)).thenReturn(bookInBase);

		// when
		ResultActions resultActions = mockMvc.perform(get("/books/book/").param("id", "1"));

		// then
		resultActions.andExpect(view().name("book"))
				.andExpect(model().attribute(ModelConstants.BOOK, new ArgumentMatcher<Object>() {
					@Override
					public boolean matches(Object argument) {
						BookTo book = (BookTo) argument;
						return book != null && bookInBase.getTitle().equals(book.getTitle());
					}
				}));
	}

	@Test
	public void shouldTestGetForFoundBooks() throws Exception {

		// given
		List<BookTo> testList = new ArrayList<BookTo>();
		BookTo testBook = new BookTo();
		testBook.setId(1L);
		testBook.setTitle("testBook");
		testBook.setAuthors("testAuthor");
		testBook.setStatus(BookStatus.FREE);
		testList.add(testBook);
		Mockito.when(bookService.findBooksByAuthorOrTitle("", "testBook")).thenReturn(testList);

		// when
		ResultActions resultActions = mockMvc
				.perform(get("/books/found/").param("titlePrefix", "testBook").param("authorPrefix", ""));
		// then
		resultActions.andExpect(view().name("books"))
				.andExpect(model().attribute(ModelConstants.BOOK_LIST, new ArgumentMatcher<Object>() {
					@Override
					public boolean matches(Object argument) {
						List<BookTo> list = (List<BookTo>) argument;
						return list != null && testList.get(0).getTitle().equals(list.get(0).getTitle());
					}
				}));
	}

	@Test
	public void shouldTestGetForRemoveBook() throws Exception {

		// given
		BookTo book = new BookTo();
		book.setId(1L);
		Mockito.when(bookService.findBookById(1L)).thenReturn(book);

		// when
		ResultActions resultActions = mockMvc.perform(get("/books/remove/").param("id", "1"));

		// then
		resultActions.andExpect(view().name("removedBook")).andExpect(model().attribute(ModelConstants.BOOK, book));
	}

	@Test
	public void shouldThrowErrorViewWhenTryToRemovingBookWhichDoesntExist() throws Exception {

		// given
		List<BookTo> list = new ArrayList<BookTo>();
		Mockito.when(bookService.findAllBooks()).thenReturn(list);

		// when
		ResultActions resultActions = mockMvc.perform(get("/books/remove/").param("id", "1"));

		// then
		resultActions.andExpect(view().name("403"))
				.andExpect(model().attribute(ModelConstants.ERROR_MESSAGE, "This book doesn't exist."));
	}

	@Test
	public void shouldThrowErrorViewWhenTryToShowDetailsBookWhichDoesntExist() throws Exception {

		// when
		ResultActions resultActions = mockMvc.perform(get("/books/book/").param("id", "1"));

		// then
		resultActions.andExpect(view().name("403"))
				.andExpect(model().attribute(ModelConstants.ERROR_MESSAGE, "This book doesn't exist."));
	}

	@Test
	public void shouldTestGetForAddBook() throws Exception {
		// given when
		ResultActions resultActions = mockMvc.perform(get("/books/add"));
		// then
		resultActions.andExpect(view().name("addBook"));
	}

	@Test
	public void shouldTestPostForAddBook() throws Exception {

		// given
		BookTo newBook = new BookTo();
		newBook.setTitle("Pan Tadeusz");
		newBook.setAuthors("Adam Mickiewicz");
		newBook.setStatus(BookStatus.FREE);

		BookTo addedBook = new BookTo();
		addedBook.setId(1L);
		addedBook.setTitle("Pan Tadeusz");
		addedBook.setAuthors("Adam Mickiewicz");
		addedBook.setStatus(BookStatus.FREE);

		Mockito.when(bookService.addNewBook(Mockito.any())).thenReturn(addedBook);

		// when
		ResultActions resultActions = this.mockMvc
				.perform(post("/books/add").flashAttr(ModelConstants.NEW_BOOK, newBook));

		// then
		resultActions.andExpect(status().isOk()).andExpect(view().name("books"))
				.andExpect(model().attribute(ModelConstants.NEW_BOOK, new ArgumentMatcher<Object>() {
					@Override
					public boolean matches(Object argument) {
						BookTo book = (BookTo) argument;
						return book != null && newBook.getTitle().equals(book.getTitle());
					}
				}));
	}
}
