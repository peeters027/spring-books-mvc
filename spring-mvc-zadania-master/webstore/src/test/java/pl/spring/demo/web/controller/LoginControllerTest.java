package pl.spring.demo.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pl.spring.demo.controller.LoginController;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private LoginController loginController;

	@Before
	public void setup() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix("jsp");

		this.mockMvc = MockMvcBuilders.standaloneSetup(loginController).setViewResolvers(viewResolver).build();
	}

	@Test
	public void testLogoutView() throws Exception {

		// when
		ResultActions resultActions = mockMvc.perform(get("/logout"));

		// then
		resultActions.andExpect(view().name("login"));
	}

	@Test
	public void testViewAfterFailedLogging() throws Exception {

		// when
		ResultActions resultActions = mockMvc.perform(get("/loginfailed"));

		// then
		resultActions.andExpect(view().name("login"));
	}
	
	@Test
	@Ignore
	public void testAccesDenied() throws Exception {

		// when
		ResultActions resultActions = mockMvc.perform(get("/403"));

		// then
		resultActions.andExpect(view().name("403"));
	}
}